from ortools.linear_solver import pywraplp
import sys
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import filedialog
from openpyxl import load_workbook
import math
import platform


def _style_code():
    style = ttk.Style()
    style.theme_use('default')
    style.configure('.', font="TkDefaultFont")
    if sys.platform == "win32":
        style.theme_use('winnative')
    _style_code_ran = 1


class Toplevel1:
    def __init__(self, top=None):

        top.geometry("804x450+660+210")
        top.minsize(120, 1)
        top.maxsize(1924, 1061)
        top.resizable(1, 1)
        top.title("М постановка")
        top.configure(background="#d9d9d9")
        top.configure(highlightbackground="#d9d9d9")
        top.configure(highlightcolor="#000000")

        self.top = top

        self.Button1 = tk.Button(self.top)
        self.Button1.place(relx=0.033, rely=0.022, height=26)
        self.Button1.configure(activebackground="#4d4d4d")
        self.Button1.configure(activeforeground="white")
        self.Button1.configure(background="#d9d9d9")
        self.Button1.configure(disabledforeground="#a3a3a3")
        self.Button1.configure(foreground="#000000")
        self.Button1.configure(highlightbackground="#d9d9d9")
        self.Button1.configure(highlightcolor="black")
        self.Button1.configure(borderwidth=2)
        self.Button1.configure(relief="raised")
        self.Button1.configure(text='''Выбор файла''', command=self.from_excel)

        self.Button2 = tk.Button(self.top)
        self.Button2.place(relx=0.2, rely=0.022, height=26)
        self.Button2.configure(activebackground="#d9d9d9")
        self.Button2.configure(activeforeground="black")
        self.Button2.configure(background="#d9d9d9")
        self.Button2.configure(disabledforeground="#a3a3a3")
        self.Button2.configure(foreground="#000000")
        self.Button2.configure(highlightbackground="#d9d9d9")
        self.Button2.configure(highlightcolor="#000000")
        self.Button2.configure(text='''Применить''', command=self.execute_multiple_commands)

        self.Label1 = tk.Label(self.top)
        self.Label1.place(relx=0.017, rely=0.156, height=21, width=40)
        self.Label1.configure(activebackground="#d9d9d9")
        self.Label1.configure(activeforeground="black")
        self.Label1.configure(anchor='w')
        self.Label1.configure(background="#d9d9d9")
        self.Label1.configure(compound='left')
        self.Label1.configure(disabledforeground="#a3a3a3")
        self.Label1.configure(foreground="#000000")
        self.Label1.configure(highlightbackground="#d9d9d9")
        self.Label1.configure(highlightcolor="#000000")
        self.Label1.configure(text='''a/11''')

        self.Label2 = tk.Label(self.top)
        self.Label2.place(relx=0.017, rely=0.244, height=21, width=40)
        self.Label2.configure(activebackground="#d9d9d9")
        self.Label2.configure(activeforeground="black")
        self.Label2.configure(anchor='w')
        self.Label2.configure(background="#d9d9d9")
        self.Label2.configure(compound='left')
        self.Label2.configure(disabledforeground="#a3a3a3")
        self.Label2.configure(foreground="#000000")

        self.Label2.configure(highlightbackground="#d9d9d9")
        self.Label2.configure(highlightcolor="#000000")
        self.Label2.configure(text='''o11''')

        self.Label4 = tk.Label(self.top)
        self.Label4.place(relx=0.017, rely=0.332, height=21, width=40)
        self.Label4.configure(activebackground="#d9d9d9")
        self.Label4.configure(activeforeground="black")
        self.Label4.configure(anchor='w')
        self.Label4.configure(background="#d9d9d9")
        self.Label4.configure(compound='left')
        self.Label4.configure(disabledforeground="#a3a3a3")
        self.Label4.configure(foreground="#000000")

        self.Label4.configure(highlightbackground="#d9d9d9")
        self.Label4.configure(highlightcolor="#000000")
        self.Label4.configure(text='''a/12''')

        self.Label5 = tk.Label(self.top)
        self.Label5.place(relx=0.017, rely=0.420, height=21, width=40)
        self.Label5.configure(activebackground="#d9d9d9")
        self.Label5.configure(activeforeground="black")
        self.Label5.configure(anchor='w')
        self.Label5.configure(background="#d9d9d9")
        self.Label5.configure(compound='left')
        self.Label5.configure(disabledforeground="#a3a3a3")
        self.Label5.configure(foreground="#000000")

        self.Label5.configure(highlightbackground="#d9d9d9")
        self.Label5.configure(highlightcolor="#000000")
        self.Label5.configure(text='''o12''')

        self.Label6 = tk.Label(self.top)
        self.Label6.place(relx=0.017, rely=0.508, height=21, width=40)
        self.Label6.configure(activebackground="#d9d9d9")
        self.Label6.configure(activeforeground="black")
        self.Label6.configure(anchor='w')
        self.Label6.configure(background="#d9d9d9")
        self.Label6.configure(compound='left')
        self.Label6.configure(disabledforeground="#a3a3a3")
        self.Label6.configure(foreground="#000000")

        self.Label6.configure(highlightbackground="#d9d9d9")
        self.Label6.configure(highlightcolor="#000000")
        self.Label6.configure(text='''b1''')

        self.Label7 = tk.Label(self.top)
        self.Label7.place(relx=0.017, rely=0.596, height=21, width=40)
        self.Label7.configure(activebackground="#d9d9d9")
        self.Label7.configure(activeforeground="black")
        self.Label7.configure(anchor='w')
        self.Label7.configure(background="#d9d9d9")
        self.Label7.configure(compound='left')
        self.Label7.configure(disabledforeground="#a3a3a3")
        self.Label7.configure(foreground="#000000")

        self.Label7.configure(highlightbackground="#d9d9d9")
        self.Label7.configure(highlightcolor="#000000")
        self.Label7.configure(text='''o1''')

        self.Label8 = tk.Label(self.top)
        self.Label8.place(relx=0.017, rely=0.684, height=21, width=40)
        self.Label8.configure(activebackground="#d9d9d9")
        self.Label8.configure(activeforeground="black")
        self.Label8.configure(anchor='w')
        self.Label8.configure(background="#d9d9d9")
        self.Label8.configure(compound='left')
        self.Label8.configure(disabledforeground="#a3a3a3")
        self.Label8.configure(foreground="#000000")

        self.Label8.configure(highlightbackground="#d9d9d9")
        self.Label8.configure(highlightcolor="#000000")
        self.Label8.configure(text='''a1''')

        self.Label9 = tk.Label(self.top)
        self.Label9.place(relx=0.017, rely=0.772, height=21, width=40)
        self.Label9.configure(activebackground="#d9d9d9")
        self.Label9.configure(activeforeground="black")
        self.Label9.configure(anchor='w')
        self.Label9.configure(background="#d9d9d9")
        self.Label9.configure(compound='left')
        self.Label9.configure(disabledforeground="#a3a3a3")
        self.Label9.configure(foreground="#000000")

        self.Label9.configure(highlightbackground="#d9d9d9")
        self.Label9.configure(highlightcolor="#000000")
        self.Label9.configure(text='''a2''')

        self.Label10 = tk.Label(self.top)
        self.Label10.place(relx=0.017, rely=0.860, height=21, width=40)
        self.Label10.configure(activebackground="#d9d9d9")
        self.Label10.configure(activeforeground="black")
        self.Label10.configure(anchor='w')
        self.Label10.configure(background="#d9d9d9")
        self.Label10.configure(compound='left')
        self.Label10.configure(disabledforeground="#a3a3a3")
        self.Label10.configure(foreground="#000000")

        self.Label10.configure(highlightbackground="#d9d9d9")
        self.Label10.configure(highlightcolor="#000000")
        self.Label10.configure(text='''c1''')

        self.Label11 = tk.Label(self.top)
        self.Label11.place(relx=0.017, rely=0.948, height=21, width=40)
        self.Label11.configure(activebackground="#d9d9d9")
        self.Label11.configure(activeforeground="black")
        self.Label11.configure(anchor='w')
        self.Label11.configure(background="#d9d9d9")
        self.Label11.configure(compound='left')
        self.Label11.configure(disabledforeground="#a3a3a3")
        self.Label11.configure(foreground="#000000")

        self.Label11.configure(highlightbackground="#d9d9d9")
        self.Label11.configure(highlightcolor="#000000")
        self.Label11.configure(text='''d1''')

        self.Label12 = tk.Label(self.top)
        self.Label12.place(relx=0.267, rely=0.948, height=21, width=40)
        self.Label12.configure(activebackground="#d9d9d9")
        self.Label12.configure(activeforeground="black")
        self.Label12.configure(anchor='w')
        self.Label12.configure(background="#d9d9d9")
        self.Label12.configure(compound='left')
        self.Label12.configure(disabledforeground="#a3a3a3")
        self.Label12.configure(foreground="#000000")

        self.Label12.configure(highlightbackground="#d9d9d9")
        self.Label12.configure(highlightcolor="#000000")
        self.Label12.configure(text='''D1''')

        self.Label3 = tk.Label(self.top)
        self.Label3.place(relx=0.267, rely=0.156, height=21, width=40)
        self.Label3.configure(activebackground="#d9d9d9")
        self.Label3.configure(activeforeground="black")
        self.Label3.configure(anchor='w')
        self.Label3.configure(background="#d9d9d9")
        self.Label3.configure(compound='left')
        self.Label3.configure(disabledforeground="#a3a3a3")
        self.Label3.configure(foreground="#000000")
        self.Label3.configure(highlightbackground="#d9d9d9")
        self.Label3.configure(highlightcolor="#000000")
        self.Label3.configure(text='''a/21''')

        self.Label13 = tk.Label(self.top)
        self.Label13.place(relx=0.267, rely=0.244, height=21, width=40)
        self.Label13.configure(activebackground="#d9d9d9")
        self.Label13.configure(activeforeground="black")
        self.Label13.configure(anchor='w')
        self.Label13.configure(background="#d9d9d9")
        self.Label13.configure(compound='left')
        self.Label13.configure(disabledforeground="#a3a3a3")
        self.Label13.configure(foreground="#000000")
        self.Label13.configure(highlightbackground="#d9d9d9")
        self.Label13.configure(highlightcolor="#000000")
        self.Label13.configure(text='''o21''')

        self.Label14 = tk.Label(self.top)
        self.Label14.place(relx=0.267, rely=0.332, height=21, width=40)
        self.Label14.configure(activebackground="#d9d9d9")
        self.Label14.configure(activeforeground="black")
        self.Label14.configure(anchor='w')
        self.Label14.configure(background="#d9d9d9")
        self.Label14.configure(compound='left')
        self.Label14.configure(disabledforeground="#a3a3a3")
        self.Label14.configure(foreground="#000000")
        self.Label14.configure(highlightbackground="#d9d9d9")
        self.Label14.configure(highlightcolor="#000000")
        self.Label14.configure(text='''a/22''')

        self.Label15 = tk.Label(self.top)
        self.Label15.place(relx=0.267, rely=0.420, height=21, width=40)
        self.Label15.configure(activebackground="#d9d9d9")
        self.Label15.configure(activeforeground="black")
        self.Label15.configure(anchor='w')
        self.Label15.configure(background="#d9d9d9")
        self.Label15.configure(compound='left')
        self.Label15.configure(disabledforeground="#a3a3a3")
        self.Label15.configure(foreground="#000000")
        self.Label15.configure(highlightbackground="#d9d9d9")
        self.Label15.configure(highlightcolor="#000000")
        self.Label15.configure(text='''o22''')

        self.Label16 = tk.Label(self.top)
        self.Label16.place(relx=0.267, rely=0.508, height=21, width=40)
        self.Label16.configure(activebackground="#d9d9d9")
        self.Label16.configure(activeforeground="black")
        self.Label16.configure(anchor='w')
        self.Label16.configure(background="#d9d9d9")
        self.Label16.configure(compound='left')
        self.Label16.configure(disabledforeground="#a3a3a3")
        self.Label16.configure(foreground="#000000")
        self.Label16.configure(highlightbackground="#d9d9d9")
        self.Label16.configure(highlightcolor="#000000")
        self.Label16.configure(text='''b2''')

        self.Label17 = tk.Label(self.top)
        self.Label17.place(relx=0.267, rely=0.596, height=21, width=40)
        self.Label17.configure(activebackground="#d9d9d9")
        self.Label17.configure(activeforeground="black")
        self.Label17.configure(anchor='w')
        self.Label17.configure(background="#d9d9d9")
        self.Label17.configure(compound='left')
        self.Label17.configure(disabledforeground="#a3a3a3")
        self.Label17.configure(foreground="#000000")
        self.Label17.configure(highlightbackground="#d9d9d9")
        self.Label17.configure(highlightcolor="#000000")
        self.Label17.configure(text='''o2''')

        self.Label18 = tk.Label(self.top)
        self.Label18.place(relx=0.267, rely=0.684, height=21, width=40)
        self.Label18.configure(activebackground="#d9d9d9")
        self.Label18.configure(activeforeground="black")
        self.Label18.configure(anchor='w')
        self.Label18.configure(background="#d9d9d9")
        self.Label18.configure(compound='left')
        self.Label18.configure(disabledforeground="#a3a3a3")
        self.Label18.configure(foreground="#000000")
        self.Label18.configure(highlightbackground="#d9d9d9")
        self.Label18.configure(highlightcolor="#000000")
        self.Label18.configure(text='''c2''')

        self.Label19 = tk.Label(self.top)
        self.Label19.place(relx=0.267, rely=0.772, height=21, width=40)
        self.Label19.configure(activebackground="#d9d9d9")
        self.Label19.configure(activeforeground="black")
        self.Label19.configure(anchor='w')
        self.Label19.configure(background="#d9d9d9")
        self.Label19.configure(compound='left')
        self.Label19.configure(disabledforeground="#a3a3a3")
        self.Label19.configure(foreground="#000000")
        self.Label19.configure(highlightbackground="#d9d9d9")
        self.Label19.configure(highlightcolor="#000000")
        self.Label19.configure(text='''d2''')

        self.Label20 = tk.Label(self.top)
        self.Label20.place(relx=0.267, rely=0.860, height=21, width=40)
        self.Label20.configure(activebackground="#d9d9d9")
        self.Label20.configure(activeforeground="black")
        self.Label20.configure(anchor='w')
        self.Label20.configure(background="#d9d9d9")
        self.Label20.configure(compound='left')
        self.Label20.configure(disabledforeground="#a3a3a3")
        self.Label20.configure(foreground="#000000")
        self.Label20.configure(highlightbackground="#d9d9d9")
        self.Label20.configure(highlightcolor="#000000")
        self.Label20.configure(text='''D2''')

        self.Text1 = tk.Text(self.top)
        self.Text1.place(relx=0.099, rely=0.156, relheight=0.053, relwidth=0.141)

        self.Text1.configure(background="white")
        self.Text1.configure(font="TkTextFont")
        self.Text1.configure(foreground="black")
        self.Text1.configure(highlightbackground="#d9d9d9")
        self.Text1.configure(highlightcolor="#000000")
        self.Text1.configure(insertbackground="#000000")
        self.Text1.configure(selectbackground="#d9d9d9")
        self.Text1.configure(selectforeground="black")
        self.Text1.configure(wrap="word")

        self.Text2 = tk.Text(self.top)
        self.Text2.place(relx=0.099, rely=0.244, relheight=0.053, relwidth=0.141)

        self.Text2.configure(background="white")
        self.Text2.configure(font="TkTextFont")
        self.Text2.configure(foreground="black")
        self.Text2.configure(highlightbackground="#d9d9d9")
        self.Text2.configure(highlightcolor="#000000")
        self.Text2.configure(insertbackground="#000000")
        self.Text2.configure(selectbackground="#d9d9d9")
        self.Text2.configure(selectforeground="black")
        self.Text2.configure(wrap="word")

        self.Text4 = tk.Text(self.top)
        self.Text4.place(relx=0.099, rely=0.332, relheight=0.053, relwidth=0.141)

        self.Text4.configure(background="white")
        self.Text4.configure(font="TkTextFont")
        self.Text4.configure(foreground="black")
        self.Text4.configure(highlightbackground="#d9d9d9")
        self.Text4.configure(highlightcolor="#000000")
        self.Text4.configure(insertbackground="#000000")
        self.Text4.configure(selectbackground="#d9d9d9")
        self.Text4.configure(selectforeground="black")
        self.Text4.configure(wrap="word")

        self.Text5 = tk.Text(self.top)
        self.Text5.place(relx=0.099, rely=0.420, relheight=0.053, relwidth=0.141)

        self.Text5.configure(background="white")
        self.Text5.configure(font="TkTextFont")
        self.Text5.configure(foreground="black")
        self.Text5.configure(highlightbackground="#d9d9d9")
        self.Text5.configure(highlightcolor="#000000")
        self.Text5.configure(insertbackground="#000000")
        self.Text5.configure(selectbackground="#d9d9d9")
        self.Text5.configure(selectforeground="black")
        self.Text5.configure(wrap="word")

        self.Text6 = tk.Text(self.top)
        self.Text6.place(relx=0.099, rely=0.508, relheight=0.053, relwidth=0.141)

        self.Text6.configure(background="white")
        self.Text6.configure(font="TkTextFont")
        self.Text6.configure(foreground="black")
        self.Text6.configure(highlightbackground="#d9d9d9")
        self.Text6.configure(highlightcolor="#000000")
        self.Text6.configure(insertbackground="#000000")
        self.Text6.configure(selectbackground="#d9d9d9")
        self.Text6.configure(selectforeground="black")
        self.Text6.configure(wrap="word")

        self.Text7 = tk.Text(self.top)
        self.Text7.place(relx=0.099, rely=0.596, relheight=0.053, relwidth=0.141)

        self.Text7.configure(background="white")
        self.Text7.configure(font="TkTextFont")
        self.Text7.configure(foreground="black")
        self.Text7.configure(highlightbackground="#d9d9d9")
        self.Text7.configure(highlightcolor="#000000")
        self.Text7.configure(insertbackground="#000000")
        self.Text7.configure(selectbackground="#d9d9d9")
        self.Text7.configure(selectforeground="black")
        self.Text7.configure(wrap="word")

        self.Text8 = tk.Text(self.top)
        self.Text8.place(relx=0.099, rely=0.684, relheight=0.053, relwidth=0.141)

        self.Text8.configure(background="white")
        self.Text8.configure(font="TkTextFont")
        self.Text8.configure(foreground="black")
        self.Text8.configure(highlightbackground="#d9d9d9")
        self.Text8.configure(highlightcolor="#000000")
        self.Text8.configure(insertbackground="#000000")
        self.Text8.configure(selectbackground="#d9d9d9")
        self.Text8.configure(selectforeground="black")
        self.Text8.configure(wrap="word")

        self.Text9 = tk.Text(self.top)
        self.Text9.place(relx=0.099, rely=0.772, relheight=0.053, relwidth=0.141)

        self.Text9.configure(background="white")
        self.Text9.configure(font="TkTextFont")
        self.Text9.configure(foreground="black")
        self.Text9.configure(highlightbackground="#d9d9d9")
        self.Text9.configure(highlightcolor="#000000")
        self.Text9.configure(insertbackground="#000000")
        self.Text9.configure(selectbackground="#d9d9d9")
        self.Text9.configure(selectforeground="black")
        self.Text9.configure(wrap="word")

        self.Text10 = tk.Text(self.top)
        self.Text10.place(relx=0.099, rely=0.860, relheight=0.053, relwidth=0.141)

        self.Text10.configure(background="white")
        self.Text10.configure(font="TkTextFont")
        self.Text10.configure(foreground="black")
        self.Text10.configure(highlightbackground="#d9d9d9")
        self.Text10.configure(highlightcolor="#000000")
        self.Text10.configure(insertbackground="#000000")
        self.Text10.configure(selectbackground="#d9d9d9")
        self.Text10.configure(selectforeground="black")
        self.Text10.configure(wrap="word")

        self.Text11 = tk.Text(self.top)
        self.Text11.place(relx=0.099, rely=0.948, relheight=0.053, relwidth=0.141)

        self.Text11.configure(background="white")
        self.Text11.configure(font="TkTextFont")
        self.Text11.configure(foreground="black")
        self.Text11.configure(highlightbackground="#d9d9d9")
        self.Text11.configure(highlightcolor="#000000")
        self.Text11.configure(insertbackground="#000000")
        self.Text11.configure(selectbackground="#d9d9d9")
        self.Text11.configure(selectforeground="black")
        self.Text11.configure(wrap="word")

        self.Text12 = tk.Text(self.top)
        self.Text12.place(relx=0.349, rely=0.948, relheight=0.053, relwidth=0.141)

        self.Text12.configure(background="white")
        self.Text12.configure(font="TkTextFont")
        self.Text12.configure(foreground="black")
        self.Text12.configure(highlightbackground="#d9d9d9")
        self.Text12.configure(highlightcolor="#000000")
        self.Text12.configure(insertbackground="#000000")
        self.Text12.configure(selectbackground="#d9d9d9")
        self.Text12.configure(selectforeground="black")
        self.Text12.configure(wrap="word")

        self.Text3 = tk.Text(self.top)
        self.Text3.place(relx=0.349, rely=0.156, relheight=0.053, relwidth=0.141)

        self.Text3.configure(background="white")
        self.Text3.configure(font="TkTextFont")
        self.Text3.configure(foreground="black")
        self.Text3.configure(highlightbackground="#d9d9d9")
        self.Text3.configure(highlightcolor="#000000")
        self.Text3.configure(insertbackground="#000000")
        self.Text3.configure(selectbackground="#d9d9d9")
        self.Text3.configure(selectforeground="black")
        self.Text3.configure(wrap="word")

        self.Text13 = tk.Text(self.top)
        self.Text13.place(relx=0.349, rely=0.244, relheight=0.053, relwidth=0.141)

        self.Text13.configure(background="white")
        self.Text13.configure(font="TkTextFont")
        self.Text13.configure(foreground="black")
        self.Text13.configure(highlightbackground="#d9d9d9")
        self.Text13.configure(highlightcolor="#000000")
        self.Text13.configure(insertbackground="#000000")
        self.Text13.configure(selectbackground="#d9d9d9")
        self.Text13.configure(selectforeground="black")
        self.Text13.configure(wrap="word")

        self.Text14 = tk.Text(self.top)
        self.Text14.place(relx=0.349, rely=0.332, relheight=0.053, relwidth=0.141)

        self.Text14.configure(background="white")
        self.Text14.configure(font="TkTextFont")
        self.Text14.configure(foreground="black")
        self.Text14.configure(highlightbackground="#d9d9d9")
        self.Text14.configure(highlightcolor="#000000")
        self.Text14.configure(insertbackground="#000000")
        self.Text14.configure(selectbackground="#d9d9d9")
        self.Text14.configure(selectforeground="black")
        self.Text14.configure(wrap="word")

        self.Text15 = tk.Text(self.top)
        self.Text15.place(relx=0.349, rely=0.420, relheight=0.053, relwidth=0.141)

        self.Text15.configure(background="white")
        self.Text15.configure(font="TkTextFont")
        self.Text15.configure(foreground="black")
        self.Text15.configure(highlightbackground="#d9d9d9")
        self.Text15.configure(highlightcolor="#000000")
        self.Text15.configure(insertbackground="#000000")
        self.Text15.configure(selectbackground="#d9d9d9")
        self.Text15.configure(selectforeground="black")
        self.Text15.configure(wrap="word")

        self.Text16 = tk.Text(self.top)
        self.Text16.place(relx=0.349, rely=0.508, relheight=0.053, relwidth=0.141)

        self.Text16.configure(background="white")
        self.Text16.configure(font="TkTextFont")
        self.Text16.configure(foreground="black")
        self.Text16.configure(highlightbackground="#d9d9d9")
        self.Text16.configure(highlightcolor="#000000")
        self.Text16.configure(insertbackground="#000000")
        self.Text16.configure(selectbackground="#d9d9d9")
        self.Text16.configure(selectforeground="black")
        self.Text16.configure(wrap="word")

        self.Text17 = tk.Text(self.top)
        self.Text17.place(relx=0.349, rely=0.596, relheight=0.053, relwidth=0.141)

        self.Text17.configure(background="white")
        self.Text17.configure(font="TkTextFont")
        self.Text17.configure(foreground="black")
        self.Text17.configure(highlightbackground="#d9d9d9")
        self.Text17.configure(highlightcolor="#000000")
        self.Text17.configure(insertbackground="#000000")
        self.Text17.configure(selectbackground="#d9d9d9")
        self.Text17.configure(selectforeground="black")
        self.Text17.configure(wrap="word")

        self.Text18 = tk.Text(self.top)
        self.Text18.place(relx=0.349, rely=0.684, relheight=0.053, relwidth=0.141)

        self.Text18.configure(background="white")
        self.Text18.configure(font="TkTextFont")
        self.Text18.configure(foreground="black")
        self.Text18.configure(highlightbackground="#d9d9d9")
        self.Text18.configure(highlightcolor="#000000")
        self.Text18.configure(insertbackground="#000000")
        self.Text18.configure(selectbackground="#d9d9d9")
        self.Text18.configure(selectforeground="black")
        self.Text18.configure(wrap="word")

        self.Text19 = tk.Text(self.top)
        self.Text19.place(relx=0.349, rely=0.772, relheight=0.053, relwidth=0.141)

        self.Text19.configure(background="white")
        self.Text19.configure(font="TkTextFont")
        self.Text19.configure(foreground="black")
        self.Text19.configure(highlightbackground="#d9d9d9")
        self.Text19.configure(highlightcolor="#000000")
        self.Text19.configure(insertbackground="#000000")
        self.Text19.configure(selectbackground="#d9d9d9")
        self.Text19.configure(selectforeground="black")
        self.Text19.configure(wrap="word")

        self.Text20 = tk.Text(self.top)
        self.Text20.place(relx=0.349, rely=0.860, relheight=0.053, relwidth=0.141)

        self.Text20.configure(background="white")
        self.Text20.configure(font="TkTextFont")
        self.Text20.configure(foreground="black")
        self.Text20.configure(highlightbackground="#d9d9d9")
        self.Text20.configure(highlightcolor="#000000")
        self.Text20.configure(insertbackground="#000000")
        self.Text20.configure(selectbackground="#d9d9d9")
        self.Text20.configure(selectforeground="black")
        self.Text20.configure(wrap="word")

        _style_code()
        self.Scrolledtreeview1 = ScrolledTreeView(self.top)
        self.Scrolledtreeview1.place(relx=0.526, rely=0.022, relheight=0.32
                                     , relwidth=0.439)
        self.Scrolledtreeview1.configure(columns=("Col1", "Col2", "Col3"))
        self.Scrolledtreeview1.heading("#0", text="")
        self.Scrolledtreeview1.heading("#0", anchor="center")
        self.Scrolledtreeview1.column("#0", width="75")
        self.Scrolledtreeview1.column("#0", minwidth="20")
        self.Scrolledtreeview1.column("#0", stretch="1")
        self.Scrolledtreeview1.column("#0", anchor="w")

        self.Scrolledtreeview1.heading("Col1", text="")
        self.Scrolledtreeview1.heading("Col1", anchor="center")
        self.Scrolledtreeview1.column("Col1", width="75")
        self.Scrolledtreeview1.column("Col1", minwidth="20")
        self.Scrolledtreeview1.column("Col1", stretch="1")
        self.Scrolledtreeview1.column("Col1", anchor="w")

        self.Scrolledtreeview1.heading("Col2", text="")
        self.Scrolledtreeview1.heading("Col2", anchor="center")
        self.Scrolledtreeview1.column("Col2", width="75")
        self.Scrolledtreeview1.column("Col2", minwidth="20")
        self.Scrolledtreeview1.column("Col2", stretch="1")
        self.Scrolledtreeview1.column("Col2", anchor="w")

        self.Scrolledtreeview1.heading("Col3", text="")
        self.Scrolledtreeview1.heading("Col3", anchor="center")
        self.Scrolledtreeview1.column("Col3", width="75")
        self.Scrolledtreeview1.column("Col3", minwidth="20")
        self.Scrolledtreeview1.column("Col3", stretch="1")
        self.Scrolledtreeview1.column("Col3", anchor="w")

        self.Scrolledtreeview1.insert("", "end", text=str("1.T"))
        self.Scrolledtreeview1.insert("", "end", text=str("1.T^2"))
        self.Scrolledtreeview1.insert("", "end", text=str("2.T"))
        self.Scrolledtreeview1.insert("", "end", text=str("2.T^2"))

        self.Scrolledtreeview2 = ScrolledTreeView(self.top)
        self.Scrolledtreeview2.place(relx=0.526, rely=0.272, relheight=0.32
                                     , relwidth=0.439)
        self.Scrolledtreeview2.configure(columns=("Col1", "Col2", "Col3"))
        self.Scrolledtreeview2.heading("#0", text="")
        self.Scrolledtreeview2.heading("#0", anchor="center")
        self.Scrolledtreeview2.column("#0", width="75")
        self.Scrolledtreeview2.column("#0", minwidth="20")
        self.Scrolledtreeview2.column("#0", stretch="1")
        self.Scrolledtreeview2.column("#0", anchor="w")

        self.Scrolledtreeview2.heading("Col1", text="")
        self.Scrolledtreeview2.heading("Col1", anchor="center")
        self.Scrolledtreeview2.column("Col1", width="75")
        self.Scrolledtreeview2.column("Col1", minwidth="20")
        self.Scrolledtreeview2.column("Col1", stretch="1")
        self.Scrolledtreeview2.column("Col1", anchor="w")

        self.Scrolledtreeview2.heading("Col2", text="")
        self.Scrolledtreeview2.heading("Col2", anchor="center")
        self.Scrolledtreeview2.column("Col2", width="75")
        self.Scrolledtreeview2.column("Col2", minwidth="20")
        self.Scrolledtreeview2.column("Col2", stretch="1")
        self.Scrolledtreeview2.column("Col2", anchor="w")

        self.Scrolledtreeview2.heading("Col3", text="")
        self.Scrolledtreeview2.heading("Col3", anchor="center")
        self.Scrolledtreeview2.column("Col3", width="75")
        self.Scrolledtreeview2.column("Col3", minwidth="20")
        self.Scrolledtreeview2.column("Col3", stretch="1")
        self.Scrolledtreeview2.column("Col3", anchor="w")

        self.Scrolledtreeview2.insert("", "end", text=str("1.KX"))
        self.Scrolledtreeview2.insert("", "end", text=str("1.X^2"))
        self.Scrolledtreeview2.insert("", "end", text=str("2.K.X^2"))
        self.Scrolledtreeview2.insert("", "end", text=str("2.X^2"))

        self.Scrolledtreeview3 = ScrolledTreeView(self.top)
        self.Scrolledtreeview3.place(relx=0.526, rely=0.522, relheight=0.32
                                     , relwidth=0.439)
        self.Scrolledtreeview3.configure(columns=("Col1", "Col2", "Col3"))
        self.Scrolledtreeview3.heading("#0", text="")
        self.Scrolledtreeview3.heading("#0", anchor="center")
        self.Scrolledtreeview3.column("#0", width="75")
        self.Scrolledtreeview3.column("#0", minwidth="20")
        self.Scrolledtreeview3.column("#0", stretch="1")
        self.Scrolledtreeview3.column("#0", anchor="w")

        self.Scrolledtreeview3.heading("Col1", text="")
        self.Scrolledtreeview3.heading("Col1", anchor="center")
        self.Scrolledtreeview3.column("Col1", width="75")
        self.Scrolledtreeview3.column("Col1", minwidth="20")
        self.Scrolledtreeview3.column("Col1", stretch="1")
        self.Scrolledtreeview3.column("Col1", anchor="w")

        self.Scrolledtreeview3.heading("Col2", text="")
        self.Scrolledtreeview3.heading("Col2", anchor="center")
        self.Scrolledtreeview3.column("Col2", width="75")
        self.Scrolledtreeview3.column("Col2", minwidth="20")
        self.Scrolledtreeview3.column("Col2", stretch="1")
        self.Scrolledtreeview3.column("Col2", anchor="w")

        self.Scrolledtreeview3.heading("Col3", text="")
        self.Scrolledtreeview3.heading("Col3", anchor="center")
        self.Scrolledtreeview3.column("Col3", width="75")
        self.Scrolledtreeview3.column("Col3", minwidth="20")
        self.Scrolledtreeview3.column("Col3", stretch="1")
        self.Scrolledtreeview3.column("Col3", anchor="w")

        self.Scrolledtreeview3.insert("", "end", text=str("2.KX"))
        self.Scrolledtreeview3.insert("", "end", text=str("2.X^2"))
        self.Scrolledtreeview3.insert("", "end", text=str("1.K.X^2"))
        self.Scrolledtreeview3.insert("", "end", text=str("2.X^2"))

        self.Scrolledtreeview4 = ScrolledTreeView(self.top)
        self.Scrolledtreeview4.place(relx=0.526, rely=0.772, relheight=0.22
                                     , relwidth=0.439)
        self.Scrolledtreeview4.configure(columns=("Col1", "Col2", "Col3"))
        self.Scrolledtreeview4.heading("#0", text="ai")
        self.Scrolledtreeview4.heading("#0", anchor="center")
        self.Scrolledtreeview4.column("#0", width="75")
        self.Scrolledtreeview4.column("#0", minwidth="20")
        self.Scrolledtreeview4.column("#0", stretch="1")
        self.Scrolledtreeview4.column("#0", anchor="w")

        self.Scrolledtreeview4.heading("Col1", text="x1")
        self.Scrolledtreeview4.heading("Col1", anchor="center")
        self.Scrolledtreeview4.column("Col1", width="75")
        self.Scrolledtreeview4.column("Col1", minwidth="20")
        self.Scrolledtreeview4.column("Col1", stretch="1")
        self.Scrolledtreeview4.column("Col1", anchor="w")

        self.Scrolledtreeview4.heading("Col2", text="x2")
        self.Scrolledtreeview4.heading("Col2", anchor="center")
        self.Scrolledtreeview4.column("Col2", width="75")
        self.Scrolledtreeview4.column("Col2", minwidth="20")
        self.Scrolledtreeview4.column("Col2", stretch="1")
        self.Scrolledtreeview4.column("Col2", anchor="w")

        self.Scrolledtreeview4.heading("Col3", text="F")
        self.Scrolledtreeview4.heading("Col3", anchor="center")
        self.Scrolledtreeview4.column("Col3", width="75")
        self.Scrolledtreeview4.column("Col3", minwidth="20")
        self.Scrolledtreeview4.column("Col3", stretch="1")
        self.Scrolledtreeview4.column("Col3", anchor="w")

    def execute_multiple_commands(self):
        self.apply_x1()
        self.apply_x2()
        self.apply_T()
        self.LinearProgramming()

    def apply_x1(self):
        try:
            content3 = self.Text11.get("1.0", "end-1c").strip()
            content4 = self.Text12.get("1.0", "end-1c").strip()
            content5 = self.Text13.get("1.0", "end-1c").strip()
            print(f"Retrieved: '{content3}' and '{content4}' and '{content5}'")

            num1 = float(content3)
            num2 = float(content4)
            num3 = float(content5)
            num4 = (num1 + num2) / 2

            self.Scrolledtreeview2.delete(*self.Scrolledtreeview2.get_children())

            if num1 ** 2 <= num3:
                value3_1 = (num1 ** 2) * (num1 ** 2)
                value3_2 = (num4 ** 2) * (num1 ** 2)
                value3_3 = (num2 ** 2) * (num1 ** 2)
            else:
                value3_1 = (num1 ** 2) * num1
                value3_2 = (num4 ** 2) * num1
                value3_3 = (num2 ** 2) * num1

            if num3 < num2:
                value4_1 = (num1 ** 2) * (num1 ** 2)
                value4_2 = (num4 ** 2) * (num4 ** 2)
                value4_3 = (num2 ** 2) * (num2 ** 2)
            else:
                value4_1 = (num1 ** 2) * num2
                value4_2 = (num4 ** 2) * num2
                value4_3 = (num2 ** 2) * num2

            self.Scrolledtreeview2.insert("", "end", iid='row0', text=str("1K.X"), values=(num1, num4, num3))
            self.Scrolledtreeview2.insert("", "end", text=str("1.X^2"), values=(num1 ** 2, num4 ** 2, num3 ** 2))
            self.Scrolledtreeview2.insert("", "end", iid='row2', text=str("1K.X^2"), values=(value3_1, value3_2, value3_3))
            self.Scrolledtreeview2.insert("", "end", iid='row3', text=str("2.X^2"), values=(value4_1, value4_2, value4_3))

        except ValueError as e:
            print(f"Error: {e}")
            self.Scrolledtreeview2.delete(*self.Scrolledtreeview2.get_children())
            self.Scrolledtreeview2.insert("", "end", text="Invalid input", values=("Invalid input",))
            self.Scrolledtreeview2.insert("", "end", text="", values=("Invalid input",))

    def apply_x2(self):
        try:
            content3 = self.Text19.get("1.0", "end-1c").strip()
            content4 = self.Text20.get("1.0", "end-1c").strip()
            content5 = self.Text15.get("1.0", "end-1c").strip()
            print(f"Retrieved: '{content3}' and '{content4}' and '{content5}'")

            num1 = float(content3)
            num3 = float(content4)
            num4 = float(content5)

            num2 = (num1 + num3) / 2

            self.Scrolledtreeview3.delete(*self.Scrolledtreeview3.get_children())

            if num1 ** 2 <= num3:
                value3_1 = (num1 ** 2) * (num1 ** 2)
                value3_2 = (num2 ** 2) * (num1 ** 2)
                value3_3 = (num3 ** 2) * (num1 ** 2)
            else:
                value3_1 = (num1 ** 2) * num1
                value3_2 = (num2 ** 2) * num1
                value3_3 = (num3 ** 2) * num1

            if num4 < num3:
                value4_1 = (num1 ** 2) * (num4 ** 2)
                value4_2 = (num2 ** 2) * (num4 ** 2)
                value4_3 = (num3 ** 2) * (num4 ** 2)
            else:
                value4_1 = (num1 ** 2)
                value4_2 = (num2 ** 2)
                value4_3 = (num3 ** 2)

            self.Scrolledtreeview3.insert("", "end", iid='row0', text=str("2K.X"), values=(num1, num2, num3))
            self.Scrolledtreeview3.insert("", "end", iid='row1', text=str("2.X^2"), values=(num1 ** 2, num2 ** 2, num3 ** 2))
            self.Scrolledtreeview3.insert("", "end", iid='row2', text=str("1K.X^2"), values=(value3_1, value3_2, value3_3))
            self.Scrolledtreeview3.insert("", "end", iid='row3', text=str("2.X^2"), values=(value4_1, value4_2, value4_3))

        except ValueError as e:
            print(f"Error: {e}")
            self.Scrolledtreeview3.delete(*self.Scrolledtreeview3.get_children())
            self.Scrolledtreeview3.insert("", "end", text="Invalid input", values=("Invalid input",))
            self.Scrolledtreeview3.insert("", "end", text="", values=("Invalid input",))

    def apply_T(self):
        try:
            X4 = float(self.Scrolledtreeview2.item('row2', 'values')[0])
            X9 = float(self.Scrolledtreeview3.item('row2', 'values')[0])
            X6 = float(self.Scrolledtreeview2.item('row3', 'values')[0])
            X16 = float(self.Scrolledtreeview3.item('row3', 'values')[0])

            X4sq = float(self.Scrolledtreeview2.item('row2', 'values')[2])
            X9sq = float(self.Scrolledtreeview3.item('row2', 'values')[2])
            X6sq = float(self.Scrolledtreeview2.item('row3', 'values')[2])
            X16sq = float(self.Scrolledtreeview3.item('row3', 'values')[2])

            print("Values:", X4, X9, X6, X16)
            print("Squared Values:", X4sq, X9sq, X6sq, X16sq)

            content3 = self.Text20.get("1.0", "end-1c").strip()
            content5 = self.Text12.get("1.0", "end-1c").strip()
            content6 = self.Text19.get("1.0", "end-1c").strip()
            content7 = self.Text15.get("1.0", "end-1c").strip()
            content8 = self.Text7.get("1.0", "end-1c").strip()
            content9 = self.Text17.get("1.0", "end-1c").strip()

            print(f"Retrieved: '{content3}' and '{content5} and '{content6} and '{content7} and '{content8} and '{content9}'")

            D2 = float(content3)
            X3 = float(content5)
            X11 = float(content6)
            X22 = float(content7)
            o1 = float(content8)
            o2 = float(content9)

            x1t1 = X22 * (X3 - 1)**2
            x2t1 = D2 * ((D2 + X11) / 2 + 1)**2

            x1t2 = X3 * (X3 - 1)**2
            x2t2 = X22**2 * ((D2 + X11) / 2)**2

            t11 = math.floor(math.sqrt(X4 + X9 + o1**2))
            t1m = math.ceil(math.sqrt(x1t1 + x2t1 + o1**2))
            t12 = math.floor(math.sqrt(X4sq + X9sq + o1**2))

            t21 = math.floor(math.sqrt(X6 + X16 + o2**2))
            t2m = math.ceil(math.sqrt(x1t2 + x2t2 + o2**2))
            t22 = math.floor(math.sqrt(X6sq + X16sq + o2**2))

            self.Scrolledtreeview1.delete(*self.Scrolledtreeview1.get_children())

            self.Scrolledtreeview1.insert("", "end", iid='row0', text=str("1.T"), values=(t11, t1m, t12))
            self.Scrolledtreeview1.insert("", "end", iid='row1', text=str("1.T^2"), values=(t11 ** 2, t1m ** 2, t12 ** 2))
            self.Scrolledtreeview1.insert("", "end", iid='row2', text=str("2.T"), values=(t21, t2m, t22))
            self.Scrolledtreeview1.insert("", "end", iid='row3', text=str("2.T^2"), values=(t21 ** 2, t2m ** 2, t22 ** 2))

        except ValueError as e:
            print(f"Error: {e}")
            self.Scrolledtreeview1.delete(*self.Scrolledtreeview1.get_children())
            self.Scrolledtreeview1.insert("", "end", text="Invalid input", values=("Invalid input",))
            self.Scrolledtreeview1.insert("", "end", text="", values=("Invalid input",))

    def from_excel(self):
        file_path = filedialog.askopenfilename(
            title="Open Excel file",
            filetypes=[("Excel files", "*.xlsx *.xls")]
        )
        if file_path:
            workbook = load_workbook(file_path)
            sheet = workbook.active

            value_a1 = sheet['A1'].value
            value_b1 = sheet['B1'].value
            value_c1 = sheet['C1'].value
            value_d1 = sheet['D1'].value
            value_e1 = sheet['E1'].value
            value_f1 = sheet['F1'].value
            value_g1 = sheet['G1'].value
            value_a2 = sheet['A2'].value
            value_b2 = sheet['B2'].value
            value_c2 = sheet['C2'].value
            value_d2 = sheet['D2'].value
            value_e2 = sheet['E2'].value
            value_f2 = sheet['F2'].value
            value_g2 = sheet['G2'].value
            value_a3 = sheet['A3'].value
            value_a4 = sheet['A4'].value
            value_a5 = sheet['A5'].value
            value_b3 = sheet['B3'].value
            value_b4 = sheet['B4'].value
            value_b5 = sheet['B5'].value

            self.Text1.delete("1.0", tk.END)
            self.Text1.insert("1.0", str(value_a1) if value_a1 is not None else "")

            self.Text2.delete("1.0", tk.END)
            self.Text2.insert("1.0", str(value_b1) if value_b1 is not None else "")

            self.Text4.delete("1.0", tk.END)
            self.Text4.insert("1.0", str(value_c1) if value_c1 is not None else "")

            self.Text5.delete("1.0", tk.END)
            self.Text5.insert("1.0", str(value_d1) if value_d1 is not None else "")

            self.Text6.delete("1.0", tk.END)
            self.Text6.insert("1.0", str(value_e1) if value_e1 is not None else "")

            self.Text7.delete("1.0", tk.END)
            self.Text7.insert("1.0", str(value_f1) if value_f1 is not None else "")

            self.Text8.delete("1.0", tk.END)
            self.Text8.insert("1.0", str(value_g1) if value_g1 is not None else "")

            self.Text9.delete("1.0", tk.END)
            self.Text9.insert("1.0", str(value_g2) if value_g2 is not None else "")

            self.Text10.delete("1.0", tk.END)
            self.Text10.insert("1.0", str(value_a3) if value_a3 is not None else "")

            self.Text11.delete("1.0", tk.END)
            self.Text11.insert("1.0", str(value_a4) if value_a4 is not None else "")

            self.Text12.delete("1.0", tk.END)
            self.Text12.insert("1.0", str(value_a5) if value_a5 is not None else "")

            self.Text3.delete("1.0", tk.END)
            self.Text3.insert("1.0", str(value_a2) if value_a2 is not None else "")

            self.Text13.delete("1.0", tk.END)
            self.Text13.insert("1.0", str(value_b2) if value_b2 is not None else "")

            self.Text14.delete("1.0", tk.END)
            self.Text14.insert("1.0", str(value_c2) if value_c2 is not None else "")

            self.Text15.delete("1.0", tk.END)
            self.Text15.insert("1.0", str(value_d2) if value_d2 is not None else "")

            self.Text16.delete("1.0", tk.END)
            self.Text16.insert("1.0", str(value_e2) if value_e2 is not None else "")

            self.Text17.delete("1.0", tk.END)
            self.Text17.insert("1.0", str(value_f2) if value_f2 is not None else "")

            self.Text18.delete("1.0", tk.END)
            self.Text18.insert("1.0", str(value_b3) if value_b3 is not None else "")

            self.Text19.delete("1.0", tk.END)
            self.Text19.insert("1.0", str(value_b4) if value_b4 is not None else "")

            self.Text20.delete("1.0", tk.END)
            self.Text20.insert("1.0", str(value_b5) if value_b5 is not None else "")

    def LinearProgramming(self):
        solver = pywraplp.Solver.CreateSolver("GLOP")
        if not solver:
            return

        content1 = self.Text10.get("1.0", "end-1c").strip()
        content2 = self.Text18.get("1.0", "end-1c").strip()
        content3 = self.Text1.get("1.0", "end-1c").strip()
        content4 = self.Text4.get("1.0", "end-1c").strip()
        content5 = self.Text6.get("1.0", "end-1c").strip()
        content6 = self.Text16.get("1.0", "end-1c").strip()
        content7 = self.Text11.get("1.0", "end-1c").strip()
        content8 = self.Text19.get("1.0", "end-1c").strip()
        content9 = self.Text12.get("1.0", "end-1c").strip()
        content10 = self.Text20.get("1.0", "end-1c").strip()
        content11 = self.Text3.get("1.0", "end-1c").strip()
        content12 = self.Text14.get("1.0", "end-1c").strip()
        contentA1 = self.Text8.get("1.0", "end-1c").strip()
        contentA2 = self.Text9.get("1.0", "end-1c").strip()

        T11 = float(self.Scrolledtreeview1.item('row0', 'values')[0])
        T12 = float(self.Scrolledtreeview1.item('row0', 'values')[2])
        T115 = float(self.Scrolledtreeview1.item('row0', 'values')[1])

        T1 = float(self.Scrolledtreeview1.item('row2', 'values')[0])
        T2 = float(self.Scrolledtreeview1.item('row2', 'values')[2])
        T15 = float(self.Scrolledtreeview1.item('row2', 'values')[1])

        T31 = float(self.Scrolledtreeview1.item('row1', 'values')[0])
        T32 = float(self.Scrolledtreeview1.item('row1', 'values')[1])
        T33 = float(self.Scrolledtreeview1.item('row1', 'values')[2])

        T41 = float(self.Scrolledtreeview1.item('row3', 'values')[0])
        T42 = float(self.Scrolledtreeview1.item('row3', 'values')[1])
        T43 = float(self.Scrolledtreeview1.item('row3', 'values')[2])

        two = float(self.Scrolledtreeview2.item('row0', 'values')[0])
        four = float(self.Scrolledtreeview2.item('row0', 'values')[1])
        six = float(self.Scrolledtreeview2.item('row0', 'values')[2])

        three = float(self.Scrolledtreeview3.item('row0', 'values')[0])
        six2 = float(self.Scrolledtreeview3.item('row0', 'values')[1])
        nine = float(self.Scrolledtreeview3.item('row0', 'values')[2])

        sixteen = float(self.Scrolledtreeview2.item('row2', 'values')[0])
        sixtyfour = float(self.Scrolledtreeview2.item('row2', 'values')[1])
        hundr44 = float(self.Scrolledtreeview2.item('row2', 'values')[2])

        l24 = float(self.Scrolledtreeview2.item('row3', 'values')[0])
        l96 = float(self.Scrolledtreeview2.item('row3', 'values')[1])
        l216 = float(self.Scrolledtreeview2.item('row3', 'values')[2])

        l81 = float(self.Scrolledtreeview3.item('row2', 'values')[0])
        l324 = float(self.Scrolledtreeview3.item('row2', 'values')[1])
        l729 = float(self.Scrolledtreeview3.item('row2', 'values')[2])

        l144 = float(self.Scrolledtreeview3.item('row3', 'values')[0])
        l576 = float(self.Scrolledtreeview3.item('row3', 'values')[1])
        l1296 = float(self.Scrolledtreeview3.item('row3', 'values')[2])

        c1 = float(content1)
        c2 = float(content2)
        a11 = float(content3)
        a12 = float(content4)
        b1 = float(content5)
        b2 = float(content6)
        d1 = float(content7)
        d2 = float(content8)
        D1 = float(content9)
        D2 = float(content10)
        a21 = float(content11)
        a22 = float(content12)
        a1 = float(contentA1)
        a2 = float(contentA2)

        x1 = solver.NumVar(d1, D1, "x1")
        y11 = solver.NumVar(0, 1, "y11")
        t1 = solver.NumVar(T11, T12, "t1")
        x2 = solver.NumVar(d2, D2, "x2")
        y12 = solver.NumVar(0, 1, "y12")
        t2 = solver.NumVar(T1, T2, "t2")
        y21 = solver.NumVar(0, 1, "y21")
        y22 = solver.NumVar(0, 1, "y22")
        y31 = solver.NumVar(1, 1, "y31")
        y32 = solver.NumVar(0, 1, "y32")
        y41 = solver.NumVar(0, 1, "y41")
        y42 = solver.NumVar(0, 1, "y42")

        print("Number of variables =", solver.NumVariables())
        print(f"Retrieved: '{content1}' and '{content2}' and '{content3}'"
              f" and '{content4}' and '{content5}' and '{content6} and "
              f"'{content7} and '{content8} and '{content9} and "
              f"'{content10} and '{content11} and '{content12}")

        if a1 == 0.5 and a2 == 0.5:
            param = 0
        elif a1 == 0.6 and a2 == 0.6:
            param = 0.25
        elif a1 == 0.7 and a2 == 0.7:
            param = 0.5
        elif a1 == 0.77 and a2 == 0.77:
            param = 0.75
        elif a1 == 0.84 and a2 == 0.84:
            param = 1.0
        elif a1 == 0.89 and a2 == 0.89:
            param = 1.25
        elif a1 == 0.93 and a2 == 0.93:
            param = 1.5
        elif a1 == 0.96 and a2 == 0.96:
            param = 1.75
        elif a1 == 0.98 and a2 == 0.98:
            param = 2.0
        elif a1 == 0.987 and a2 == 0.987:
            param = 2.25
        elif a1 == 0.994 and a2 == 0.994:
            param = 2.5
        else:
            param = 0

        solver.Add(a11 * x1 + a12 * x2 + param * t1 <= b1)

        solver.Add(a21 * x1 + a22 * x2 + param * t2 <= b2)

        solver.Add((sixtyfour - sixteen) * y11 + (hundr44 - sixtyfour) * y12 + (l324 - l81) * y21 + (l729 - l324) * y22 - (T32 - T31) * y31 - (T33 - T32) * y32 <= (l81 + l81 + sixteen - hundr44))

        solver.Add((l96 - l24) * y11 + (l216 - l96) * y12 + (l576 - l144) * y21 + (l1296 - l576) * y22 - (T42 - T41) * y41 - (T43 - T42) * y42 <= T43 - (hundr44 + hundr44))

        solver.Add(x1 - (four - two) * y11 - (six - four) * y12 <= two)

        solver.Add(x2 - (six2 - three) * y21 - (nine - six2) * y22 <= three)

        solver.Add(t1 - (T115 - T11) * y31 - (T12 - T115) * y32 <= T11)

        solver.Add(t2 - T11 * y41 - (T15 - T12) * y42 <= T1)

        print("Number of constraints =", solver.NumConstraints())

        solver.Maximize(c1 * x1 + c2 * x2)

        print(f"Solving with {solver.SolverVersion()}")
        status = solver.Solve()

        if status == pywraplp.Solver.OPTIMAL:
            print("Solution:")
            print(f"Objective value = {solver.Objective().Value():0.3f}")
            print(f"x = {x1.solution_value():0.3f}")
            print(f"y = {x2.solution_value():0.3f}")

        else:
            print("The problem does not have an optimal solution.")

        print("\nAdvanced usage:")
        print(f"Problem solved in {solver.wall_time():d} milliseconds")
        print(f"Problem solved in {solver.iterations():d} iterations")
        self.Scrolledtreeview4.insert("", "end", text=a1, values=(f"{x1.solution_value():0.3f}", f"{x2.solution_value():0.3f}", f"{solver.Objective().Value():0.3f}"))


class AutoScroll(object):

    def __init__(self, master):
        try:
            vsb = ttk.Scrollbar(master, orient='vertical', command=self.yview)
        except:
            pass
        hsb = ttk.Scrollbar(master, orient='horizontal', command=self.xview)
        try:
            self.configure(yscrollcommand=self._autoscroll(vsb))
        except:
            pass
        self.configure(xscrollcommand=self._autoscroll(hsb))
        self.grid(column=0, row=0, sticky='nsew')
        try:
            vsb.grid(column=1, row=0, sticky='ns')
        except:
            pass
        hsb.grid(column=0, row=1, sticky='ew')
        master.grid_columnconfigure(0, weight=1)
        master.grid_rowconfigure(0, weight=1)
        methods = tk.Pack.__dict__.keys() | tk.Grid.__dict__.keys() \
                  | tk.Place.__dict__.keys()
        for meth in methods:
            if meth[0] != '_' and meth not in ('config', 'configure'):
                setattr(self, meth, getattr(master, meth))

    @staticmethod
    def _autoscroll(sbar):
        def wrapped(first, last):
            first, last = float(first), float(last)
            if first <= 0 and last >= 1:
                sbar.grid_remove()
            else:
                sbar.grid()
            sbar.set(first, last)

        return wrapped

    def __str__(self):
        return str(self.master)


def _create_container(func):
    def wrapped(cls, master, **kw):
        container = ttk.Frame(master)
        container.bind('<Enter>', lambda e: _bound_to_mousewheel(e, container))
        container.bind('<Leave>', lambda e: _unbound_to_mousewheel(e, container))
        return func(cls, container, **kw)

    return wrapped


class ScrolledTreeView(AutoScroll, ttk.Treeview):
    @_create_container
    def __init__(self, master, **kw):
        ttk.Treeview.__init__(self, master, **kw)
        AutoScroll.__init__(self, master)


def _bound_to_mousewheel(event, widget):
    child = widget.winfo_children()[0]
    if platform.system() == 'Windows' or platform.system() == 'Darwin':
        child.bind_all('<MouseWheel>', lambda e: _on_mousewheel(e, child))
        child.bind_all('<Shift-MouseWheel>', lambda e: _on_shiftmouse(e, child))
    else:
        child.bind_all('<Button-4>', lambda e: _on_mousewheel(e, child))
        child.bind_all('<Button-5>', lambda e: _on_mousewheel(e, child))
        child.bind_all('<Shift-Button-4>', lambda e: _on_shiftmouse(e, child))
        child.bind_all('<Shift-Button-5>', lambda e: _on_shiftmouse(e, child))


def _unbound_to_mousewheel(event, widget):
    if platform.system() == 'Windows' or platform.system() == 'Darwin':
        widget.unbind_all('<MouseWheel>')
        widget.unbind_all('<Shift-MouseWheel>')
    else:
        widget.unbind_all('<Button-4>')
        widget.unbind_all('<Button-5>')
        widget.unbind_all('<Shift-Button-4>')
        widget.unbind_all('<Shift-Button-5>')


def _on_mousewheel(event, widget):
    if platform.system() == 'Windows':
        widget.yview_scroll(-1 * int(event.delta / 120), 'units')
    elif platform.system() == 'Darwin':
        widget.yview_scroll(-1 * int(event.delta), 'units')
    else:
        if event.num == 4:
            widget.yview_scroll(-1, 'units')
        elif event.num == 5:
            widget.yview_scroll(1, 'units')


def _on_shiftmouse(event, widget):
    if platform.system() == 'Windows':
        widget.xview_scroll(-1 * int(event.delta / 120), 'units')
    elif platform.system() == 'Darwin':
        widget.xview_scroll(-1 * int(event.delta), 'units')
    else:
        if event.num == 4:
            widget.xview_scroll(-1, 'units')
        elif event.num == 5:
            widget.xview_scroll(1, 'units')


if __name__ == "__main__":
    root = tk.Tk()
    application = Toplevel1(root)
    root.mainloop()
